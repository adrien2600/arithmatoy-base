#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhss, const char *rhss) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }
  
  char * lhs = malloc(sizeof(char) * strlen(lhss) + 1);
  strcpy(lhs, lhss);
  char * rhs = malloc(sizeof(char) * strlen(rhss) + 1);
  strcpy(rhs, rhss);
  
  //printf("l: %s, r: %s\n", lhs, rhs);
  reverse(lhs);
  reverse(rhs);
  
  
  int llen = strlen(lhs);
  int rlen = strlen(rhs);
  int biggest = llen;
  if(rlen > biggest)
    biggest = rlen;
  char* result = malloc(sizeof(char) * biggest + 2);
  
  int carry = 0;
  int i = 0;
  while(i < llen || i < rlen){
    int l = 0;
    if(i < llen)
        l = get_digit_value(lhs[i]);
    
    int r = 0;
    if(i < rlen)
        r = get_digit_value(rhs[i]);
    
    if (VERBOSE)
        printf("add: digit %d, digit %d, carry %d\n", l, r, carry);
    
    int sum = l + r + carry;
    carry = sum / base;
    sum = sum % base;
    
    if (VERBOSE)
        printf("add: result: digit %d, carry %d\n", sum, carry);
    
    result[i] = to_digit(sum);
    i++;
  }
  if(carry){
    //printf("adding carry\n");
    result[i] = to_digit(carry);
    i++;
  }
  reverse(result);
  
  free(lhs);
  free(rhs);
  
  //printf("result: %s\n", result);
  return drop_leading_zeros(result);
}

char *arithmatoy_sub(unsigned int base, const char *lhss, const char *rhss) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  rhss = drop_leading_zeros(rhss);
  
  int llen = strlen(lhss);
  int rlen = strlen(rhss);
  
  char * lhs = malloc(sizeof(char) * llen + 1);
  strcpy(lhs, lhss);
  char * rhs = malloc(sizeof(char) * rlen + 1);
  strcpy(rhs, rhss);
  
  if(llen < rlen)
    return NULL;
  if(llen == rlen && strcmp(lhs, rhs) < 0)
    return NULL;
  
  //printf("l: %s, r: %s\n", lhs, rhs);
  reverse(lhs);
  reverse(rhs);

  char* result = malloc(sizeof(char) * llen + 2);
  
  int carry = 0;
  int i = 0;
  while(i < llen){
    int l = get_digit_value(lhs[i]);
    
    int r = 0;
    if(i < rlen)
      r = get_digit_value(rhs[i]);
    
    if (VERBOSE)
      printf("sub: digit %d, digit %d, carry %d\n", l, r, carry);
    
    int sub = 0;
    if(l < r + carry){
        sub = (l + base) - r - carry;
        carry = 1;
    }
    else {
        sub = l - r - carry;
        carry = 0;
    }
    
    if (VERBOSE)
        printf("sub: result: digit %d, carry %d\n", sub, carry);
    
    result[i] = to_digit(sub);
    i++;
  }

  reverse(result);

  free(lhs);
  free(rhs);
  
  //printf("result: %s\n", result);
  return drop_leading_zeros(result);
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (0 && VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
